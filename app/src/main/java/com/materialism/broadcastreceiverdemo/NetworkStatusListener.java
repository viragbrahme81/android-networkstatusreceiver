package com.materialism.broadcastreceiverdemo;

public interface NetworkStatusListener {
    void onNetworkAvailable();
    void onNetworkUnavailable();
}
