package com.materialism.broadcastreceiverdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStatusReceiver extends BroadcastReceiver {

    private NetworkStatusListener networkStatusListener;

    public NetworkStatusReceiver(NetworkStatusListener networkStatusListener) {
        this.networkStatusListener = networkStatusListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.getState()== NetworkInfo.State.CONNECTED) {
                    networkStatusListener.onNetworkAvailable();
                } else {
                    networkStatusListener.onNetworkUnavailable();
                }
            }
        }
    }
}
