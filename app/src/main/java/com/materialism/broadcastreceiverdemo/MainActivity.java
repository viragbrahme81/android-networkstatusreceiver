package com.materialism.broadcastreceiverdemo;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements NetworkStatusListener {

    private NetworkStatusReceiver networkStatusReceiver;
    private ImageView imageViewNetworkAvailable, imageViewNetworkUnavailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        if (intentFilter != null) {
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            networkStatusReceiver = new NetworkStatusReceiver(this);
            registerReceiver(networkStatusReceiver, intentFilter);
        }
    }

    private void initViews() {
        imageViewNetworkAvailable = (ImageView) findViewById(R.id.imageViewNetWorkAvailable);
        imageViewNetworkUnavailable = (ImageView) findViewById(R.id.imageViewNetworkUnavailable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (networkStatusReceiver != null) {
            unregisterReceiver(networkStatusReceiver);
        }
    }

    @Override
    public void onNetworkAvailable() {
        imageViewNetworkUnavailable.setVisibility(View.GONE);
        imageViewNetworkAvailable.setVisibility(View.VISIBLE);

    }

    @Override
    public void onNetworkUnavailable() {
        imageViewNetworkAvailable.setVisibility(View.GONE);
        imageViewNetworkUnavailable.setVisibility(View.VISIBLE);
    }
}
