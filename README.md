# README #

** Get the status of the network either wifi or 2G/3G/4G using NetworkStatusReceiver class.**

Future Scope:

1. Get the Connecting and Disconnecting state of the network.
2. Write a common logic for all the activities to register the NetworkStatusReceiver.
3. Optimise the code.